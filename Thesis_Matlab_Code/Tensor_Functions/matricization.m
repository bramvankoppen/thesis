function S = matricization(A,n)
% S = matricization(A,n) computes the matricization or unfolding of a
% d-dimensional tensor
%
% Source: Tensor decompositions and applications - T.G. Kolda and B.W.
% Bader (2009)
%
% INPUT
% A : d-dimensional tensor
% n : dimension over which the tensor is unfolded
% OUTPUT
% S : matrix which is n-dimensional unfolding of A

% Dimension vector
dimvec = size(A); 

if (length(dimvec)<n || n<1)
    error('matricization: n is not within the order range of tensor A')
end

% Shift dimensions of A, put mode n in front
Ashift = shiftdim(A,n-1);

% Compute n-mode matricization
S = reshape(Ashift,[dimvec(n), prod(dimvec(:))/dimvec(n)]);
end


