function Y = outerproduct(X)
% Y = outerproduct(X) computes the outer products of a set X of vectors,
% forming a tensor
%
% Source: Efficient MATLAB computations with sparse and factored tensors - 
% T.G. Kolda and B.W. Bader (2006)
%
% INPUT
% X : Cell with a number of d vectors
% OUTPUT
% Y : d-dimensional tensor

% Compute dimension of vector cell
n = length(X);

% Iterate for each cell
for i = 1:n
    
    % Flip cell order
    P = flip(X);

    % Save vector length
    S(i) = length(X{i});
    
    % Compute outer product
    if (i == 1) || (i == 2)
    Y = kron(P{1},P{2});
    else
    Y = kron(Y,P{i});
    end
end

% Reshape to desired dimensions
Y = reshape(Y,S);

end