function S = innerproduct(A,B)
% S = innerproduct(A,B) computes the inner product of two tensors of the
% same size
% 
% Source: Tensor decompositions and applications - T.G. Kolda and B.W.
% Bader (2009)
%
% INPUT
% A : d-dimensional tensor A
% B : d-dimensional tensor B (with equal dimensions)
% OUTPUT
% S : scalar of the inner product between A and B

% Check size of tensors
dimvec1 = size(A);
dimvec2 = size(B);

if (isequal(dimvec1,dimvec2) == 0)
    error('inprod: innerproduct not possible, tensors not same size')
end

% Vectorize both tensors
A2v = A(:);
B2v = B(:);

% Compute solution
S = A2v'*B2v;
end