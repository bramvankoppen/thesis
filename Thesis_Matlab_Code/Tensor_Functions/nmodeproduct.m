function S = nmodeproduct(A,M,n)
% Y = nmodeproduct(X) computes the n-mode product of a tensor and a matrix
%
% Source: Tensor decompositions and applications - T.G. Kolda and B.W.
% Bader (2009)
%
% INPUT
% A : d-dimensional tensor
% M : matrix where dimension 2 is equal to dimension n of tensor
% n : n-th dimension of tensor A
% OUTPUT
% S : d-dimensional tensor where n-th dimension is now equal to first
% dimension of M

% Save dimensions of A
dimvec = size(A);

if (length(dimvec)<n || n<1)
    error('nmodeproduct: n is not within the order range of tensor A');
end
if (size(M,2) ~= dimvec(n))
    error('nmodeproduct: dimension n of tensor A is not equal to dimension 2 of matrix M');
end

% Compute product of matrix with matricized tensor along dimension n
S = M*matricization(A,n);

% Replace n-th dimension and convert back into tensor with target dimensions
dimvec(n) = size(M,1);
S = reshape(S,dimvec);

% Shift dimensions back to original configuration
S = shiftdim(S,length(dimvec)-n+1);
end


