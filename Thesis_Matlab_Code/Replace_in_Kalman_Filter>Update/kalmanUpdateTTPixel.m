function [x,P,W_new] = kalmanUpdateTTPixel(x,y,C,P,W_old,Options)
% 
% ### TAKEN AND ADAPTED FROM S.J.S. DE ROOIJ ###
% This function is taken from the GitLab repository of S.J.S. de Rooij to
% match the implementation of the speedup methods.
%
% Modification: added switch cases with randomized rounding methods
%
% kalmanUpdateTTPixel(x,y,C,P_old,W,Options) calculates the Kalman update 
%   for the given parameters. In this function the parameters are given as 
%   tensor-trains.
%
%INPUT
%   x : state-vector (as tensor-train).
%   y : measured pixels (of this time-step).
%   C : measurement indices.
%   P : covariance matrix previous time-step.
%   W_old : process covariance matrix.
%   Options : Kalman filter Options struct.
%OUTPUT
%   x : updated state-vector.
%   P : updated covariance matrix.
%   W_new : (updated) process covariance.

%% Set Options
rankX = Options.Rank.x;
rankP = Options.Rank.P;
szX = size(x);
lengthX = prod(szX(:,2));
RTT = Options.R.TT;
LTT = Options.L.TT;
P_RTT = Options.P.RTT;
P_LTT = Options.P.LTT;
roundingMethod = Options.rounding.method;

if Options.W.Update
    % copy data
    W_new.W0 = W_old.W0;
    % calculate W
    W_old = W_old.varW * W_old.W0;
end

%% Prediction step
x_old = x;
P = P + W_old;
P = rounding(P,rankP,'r');

numOutputs = length(C);
v = zeros(numOutputs,1);

for ii = 1:numOutputs
    %% Prediction
    % output residual
    measIndex = C(ii);
    x_ii = extractElementTT(x,measIndex);
    v(ii,1) = y(ii,1) - x_ii;  % scalar

    % Kalman steps
    PC = extractColTTm(P,measIndex); % P*C'
    S = extractElementTT(PC,measIndex); % S scalar 
    
    K = PC*(1/S);     % rank K = rank PC 
    KSK = (K*K)*(-S); % -K*K' * S (S is scalar)

   %% Update (summation)
   x = x + K*v(ii,1);
   P = P + KSK;
 
switch roundingMethod
    case 'Det'
    if any(rank(x)>Options.Rank.maxX)   
       x = rounding(x,rankX,'r');                      % Deterministic rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = rounding(P,rankP,'r');                      % Deterministic rounding
    end
    
    case 'OtR'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,rankX,[],[],'OtR');      % Randomized rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,rankP,[],[],'OtR');  % Randomized rounding
    end

    case 'RtO'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,[],LTT,[],'RtO');      % Randomized rounding
    end  
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,[],P_LTT,[],'RtO');  % Randomized rounding
    end

    case 'TSR'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,[],RTT,LTT,'TSR');      % Randomized rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,[],P_RTT,P_LTT,'TSR');  % Randomized rounding
    end

end
end

x = rounding(x,rankX,'r');
P = rounding(P,rankP,'r');

if Options.W.Update
    % save new W
    warning off;
    W_new.varW = (1/(lengthX-1))*norm(x-x_old)^2;
    warning on;
else 
    W_new = W_old;
end
end
