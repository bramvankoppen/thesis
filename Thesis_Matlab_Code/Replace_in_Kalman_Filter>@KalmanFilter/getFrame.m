function X = getFrame(obj,sz)
% 
% ### TAKEN AND ADAPTED FROM S.J.S. DE ROOIJ ###
% This function is taken from the GitLab repository of S.J.S. de Rooij to
% match the implementation of the speedup methods.
%
% Modification: added 'block' case

%getFrame(obj) outputs the current frame as a vector in case of TT the
%   TT is converted to a vector.

if obj.Color
    numLayers = 3;
else
    numLayers = 1;
end
X = zeros(sz,numLayers);
for i = 1:numLayers

    switch obj.Options.Type
        case 'element'
            X(:,i) = TT2Vec(obj.x{i});
        case 'block'
            X(:,i) = TT2Vec(obj.x{i});
        case 'full'
            X(:,i) = obj.x{i};
        otherwise
            error("Option for KalmanFilter.Options.Type not valid");
    end

end