classdef KalmanFilter 
% 
% ### TAKEN AND ADAPTED FROM S.J.S. DE ROOIJ ###
% This function is taken from the GitLab repository of S.J.S. de Rooij to
% match the implementation of the speedup methods.
%
% Modification: added 'block' case

    %% Class Properties
    properties (Access = public)
        x
        C
        y
        W 
        P
        Color
        Mask
        %measIndex
        Options %struct: TT, fixed W or not, P diag or not.
    end

    %% Class Methods
    methods
        %% Constructor
        function obj = KalmanFilter(video,varargin)
            %KalmanFilter(video,Options) OR KalmanFilter(video) creates a
            %   KalmanFilter object.
            
            obj.Color = video.Color;
            if nargin == 2
                obj.Options = varargin{1};
            elseif nargin == 1
                obj.Options = setKalmanOptions(video);
            end
            switch obj.Options.Type
                case 'element'
                    [obj.x,obj.P,obj.W,obj.y,obj.C,obj.Mask] = ...
                        initializeKalmanTT(video,obj.Options);
                % Added case: Block Update TNKF
                case 'block'
                    [obj.x,obj.P,obj.W,obj.y,obj.C,obj.Mask] = ...
                        initializeKalmanTT(video,obj.Options);
                case 'full'
                    [obj.x,obj.P,obj.W,obj.y,obj.C,obj.Mask] = ...
                        initializeKalmanFull(video,obj.Options);
                otherwise
                    error('Options specified not valid');
            end
            
        end

        
        %% Functions
        obj = Update(obj,k);
        X = getFrame(obj,sz);

    end
end

