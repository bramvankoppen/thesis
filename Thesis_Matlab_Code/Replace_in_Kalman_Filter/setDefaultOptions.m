function [options] = setDefaultOptions(video,type,missingPixels,qHeight,qWidth,rankX,rankP,rankW, rankCols, rankXmax, rankPmax, bandWidth, RTT, LTT, P_RTT, P_LTT, blockSize, round)
% 
% ### TAKEN AND ADAPTED FROM S.J.S. DE ROOIJ ###
% This function is taken from the GitLab repository of S.J.S. de Rooij to
% match the implementation of the speedup methods.
%
% setDefaultOptions(video,missingPixels,qHeight,qWidth,rankX,rankP,rankW,bandWidth)
%   sets the Kalman filter options in a struct. You will be prompted for
%   all the remaining options.
%
%INPUT
%   video         : VideoData object
%   missingPixels : percentage missing pixels, as a fraction
%   qHeight       : quantization parameter heigth of frame, as a row-vector
%   qWidth        : quantization parameter width of frame, as row-vector
%   rankX         : maximum rank of x (video frame)
%   rankP         : maximum rank of P (state covariance matrix)
%   rankW         : rank of W (process covariance)
%   rankXmax      : maximum rank of X
%   rankPmax      : maximum rank of P
%   bandWidth     : bandwidth of W
%   RTT           : Random TT used for right-to-left partial contraction (PCRL) with state TT 
%   LTT           : Random TT used for left-to-right partial contraction (PCLR) with state TT 
%   P_RTT         : Random TT used for right-to-left partial contraction (PCRL) with covariance TT 
%   P_LTT         : Random TT used for left-to-right partial contraction (PCLR) with covariance TT 
%
%OUTPUT
%   options : struct containing all the options needed for the Kalman
%             filter.

            
if (prod(qHeight) ~= video.SizeFrame(1)) || (prod(qWidth) ~= video.SizeFrame(2))
    error('Invalid quantization parameters, try again.')
end

options.Type = type;
options.MissingPixels = missingPixels;
options.x0 = 'LastFrame'; % LastFrame or BackGround
options.q.Height = qHeight;
options.q.Width = qWidth;
options.W.Type = 'simple_dist'; % diag, simple_dist or dist
options.W.Range = bandWidth;
options.W.Update = true;
options.P.Type = 'simple_dist'; % diag, simple_dist or dist
options.P.Range = bandWidth;
options.Rank.x = rankX;
options.Rank.P = rankP;
options.Rank.W = rankW;
options.Rank.maxP = rankPmax;
options.Rank.maxX = rankXmax;
options.R.TT = RTT;
options.L.TT = LTT;
options.P.RTT = P_RTT;
options.P.LTT = P_LTT;
options.Block.Size = blockSize;
options.rounding.method = round;
options.ranks.cols = rankCols;
end