function [G,N,r] = TTSVD(A,eps)
% [G,N,r] = TTSVD(A,eps) computes the TT format of the input tensor A for
% specified accuracy epsilon.
%
% Source: Tensor-Train Decomposition - I.V. Oseledets (2011)
% 
% INPUT
%   A : tensor of specified dimensions I_{1} x I_{2} x ... x I_{d}
%   eps: desired accuracy epsilon
% OUTPUT
%   G : desired tensor in TT-format
%   N : total stored number of elements in the TT G
%   r : vector containing the ranks of G

% Order and accuracy 
n = size(A);
d = length(n);

% Frobenius norm of tensor 
AF = norm(A(:),'fro');

% Compute truncation parameter
delta = (eps/sqrt(d-1))*AF;

% Temporary tensor
C = A;

% Make rank vector
r = zeros(1,d);
r(1) = 1;

% Create cell to store cores G_{k}
G = cell(1,d);

% Perform TT-SVD algorithm
for k = 1:d-1
    C = reshape(C,r(k)*n(k),[]);
    [U,S,V] = svd(C,'econ');
    s = diag(S);
    
    x = 0;
    while norm(s(end:-1:end-x),'fro') <= delta
        x = x+1;
    end
    
    l(k) = length(s);
    r(k+1) = l(k)-x;
    G{k} = reshape(U(:,1:r(k+1)),[r(k),n(k),r(k+1)]);
    C = S(1:r(k+1),1:r(k+1))*V(:,1:r(k+1))';

    num(k) = numel(G{k});
end

% Compute stored number of elements
N = sum(num);

% Save final core
G{d} = C;
end