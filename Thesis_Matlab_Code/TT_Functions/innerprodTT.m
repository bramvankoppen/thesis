function C = innerprodTT(ATT,BTT)
% C = innerprodTT(ATT,BTT) computes the TT inner product of two tensors in
% TT format by contraction of cores
%
% Source: Tensor-Train Decomposition - I.V. Oseledets (2011)
%
% INPUT
% ATT : d-dimensional tensor in TT format with dimensions I_{1} x ... x
% I_{d}
% BTT : d-dimensional tensor in TT format with dimensions I_{1} x ... x
% I_{d}
% OUTPUT
% C : scalar value of the TT inner product between ATT and BTT

% Save length of TT
d = size(ATT,2);

% Perform algorithm in three stages
for k = 1:d
    
    % First stage: combine first two cores
    if k == 1
        A = permute(ATT{k},[2,3,1]);
        A = reshape(A,size(A,2),[]);
        
        B = permute(BTT{k},[2,3,1]);
        B = reshape(B,size(B,2),[]);
        
        C = A'*B;
    
    % Second stage: 'zipper' technique, iterate until k = d-1
    elseif (k >= 2) && (k < d)
    
        A = reshape(ATT{k},size(ATT{k},1),[]);
        A = C'*A;
        A = reshape(A,[],size(ATT{k},3));
        B = reshape(BTT{k},[],size(BTT{k},3));
        
        C = A'*B;
    
    % Final stage: combine last two cores and compute solution
    else
        
        A = C'*ATT{k};
        A = A(:);
        B = BTT{k}(:);
        
        C = A'*B; 
    end
end
