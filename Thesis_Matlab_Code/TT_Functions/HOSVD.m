function [Y,N] = HOSVD(Y,r)
% [Y,N] = HOSVD(Y,r) computes the Higher Order Singular Value
% Decomposition (HOSVD) or Tucker decomposition of a d-dimensional tensor.
%
% Source: Tensor decompositions and applications - T.G. Kolda and B.W.
% Bader (2009)
%
% INPUT
% Y : d-dimensional tensor
% t : vector with desired ranks
% OUTPUT
% Y : d-dimensional tensor in Tucker decomposition
% N : total stored number of elements in the Tucker Decomposition Y 

% Save dimensions of Y
d = ndims(Y);

for k = 1:d
    % Matricize and compute Gram matrix
    Yk = matricization(Y,k);
    Z = Yk*Yk';
    
    % Eigenvalue decomposition
    [V,D] = eig(Z);
    [~,pi] = sort(diag(D),'descend');
    
    % Save U matrices
    U{k} = V(:,pi(1:r(k)));
    num(k) = numel(U{k});
    
    % Compute n-mode product of Y and U
    Y = nmodeproduct(Y,U{k}',k);
end

N = sum(num)+numel(Y);
end