function [n,d,r] = TTsize(A)
% [n,d,r] = TTsize(A) saves the dimension, number of cores and ranks of
% tensor A in TT format.
% 
% INPUT
%   A : tensor of specified dimensions I_{1} x I_{x} x ... x I_{d}
% OUTPUT
%   n : vector with dimensions of TT I_{1} x I_{x} x ... x I_{d}
%   d : number of TT cores
%   r : vector containing the ranks of G
    
% Save core sizes
d = size(A,2);

r = zeros(1,d+1);
n = zeros(1,d);

for k = 1:d
n(k) = size(A{k},2);
r(k) = size(A{k},1);
end

r(1) = 1;
r(end) = 1;
    
end