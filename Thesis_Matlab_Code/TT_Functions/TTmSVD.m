function [G] = TTmSVD(A,dim,eps)
% [G] = TTmSVD(A,dim,eps) computes the TTm format of the input tensor A for
% specified accuracy epsilon.
% 
% Source: Tensor-Train Decomposition - I.V. Oseledets (2011)
%
% INPUT
%   A : matrix of size I x J
%   dim : vector with desired dimensions 
%   eps: desired accuracy epsilon
% OUTPUT
%   G : matrix in TTm format with desired dimensions 

% Order and accuracy 
d = length(dim)/2;

% Frobenius norm of tensor 
AF = norm(A(:),'fro');

% Compute truncation paramete r
delta = (eps/sqrt(d-1))*AF;

% Temporary tensor
C = reshape(A,dim);

% Create alternating permute order
for i = 1:d

one = i;
two = i+d;

order(i,:) = [one two];
end
order = order';
order = order(:)';

C = permute(C,order);

% Make rank vector
r = zeros(1,d);
r(1) = 1;

% Create cell to store cores G_{k}
G = cell(1,d);

% Perform TT-SVD algorithm
for k = 1:d-1
    C = reshape(C,r(k)*dim(k)*dim(k+d),[]);
    [U,S,V] = svd(C,'econ');
    s = diag(S);
    
    x = 0;
    while norm(s(end:-1:end-x),'fro') <= delta
        x = x+1;
    end
    
    r(k+1) = length(s)-x;
    G{k} = reshape(U(:,1:r(k+1)),[r(k),dim(k),dim(k+d),r(k+1)]);
    C = S(1:r(k+1),1:r(k+1))*V(:,1:r(k+1))';
end

% Save final core
G{d} = reshape(C,[],dim(d),dim(2*d));
end