function X = TTrec(G)
% X = TTrec(G) reconstructs the original vector from the TT format
% 
% INPUT
%   G : vector in TT format
% OUTPUT
%   X : original vector

d = size(G,2);

for i = 1:d-1
    SG = size(G{i});
    SG1 = size(G{i+1});
    
    if i == 1
    X = reshape(G{i},[],SG1(1));
    else
    X = X*reshape(G{i},SG(1),[]);
    X = reshape(X,[],SG1(1));
    end
end

X = X*reshape(G{end},SG1(1),[]);
end

