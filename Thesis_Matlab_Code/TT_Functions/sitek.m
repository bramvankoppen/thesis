function ATT = sitek(ATT,p)
% [ATT] = sitek(ATT,p) computes the site-k orthogonal TT format of a
% specified TT
% 
% Source: The density-matrix renormalization group in the age of matrix
% product state - Ulrich Schollwöck (2011)
%
% INPUT
% ATT : d-dimensional TT tensor
% p   : index at which to orthogonalize. All cores 1...p-1 are left
%   orthogonal and all cores p...d are right orthogonal
% OUTPUT
% ATT : p-orthogonalized version of TT-tensor A

% Save core sizes
d = size(ATT,2);

for k = 1:d
    r(k) = size(ATT{k},1);
    n(k) = size(ATT{k},2);
end

r = [r,1];

% Left-to-right orthonormalization
for k = 1:p-1
    % Left unfolding of core
    AL = reshape(ATT{k},r(k)*n(k),r(k+1));
    
    % QR factorization
    [Q,R] = qr(AL,0);

    % Save old rank
    r_old = r(k+1);

    % Compute new rank
    r(k+1) = size(Q,2);

    % Absorb R into next core
    ATT{k+1} = R*reshape(ATT{k+1},r_old,n(k+1)*r(k+2));
    
    % Reshape cores
    ATT{k} = reshape(Q,r(k),n(k),r(k+1));
    ATT{k+1} = reshape(ATT{k+1},r(k+1),n(k+1),r(k+2));
end

% Right-to-left orthonormalization
for k = d:-1:p+1
    
    % Right unfolding of core
    AR = reshape(ATT{k},r(k),n(k)*r(k+1));
    
    % QR factorization
    [Q,R] = qr(AR',0);

    % Absorb R into next core
    ATT{k-1} = reshape(ATT{k-1},n(k-1)*r(k-1),r(k))*R';

    % Compute new rank
    r(k) = size(Q',1);
    
    % Reshape cores
    ATT{k} = reshape(Q',r(k),n(k),r(k+1));    
    ATT{k-1} = reshape(ATT{k-1},r(k-1),n(k-1),r(k));
     
end
end