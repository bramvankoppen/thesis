function ATT = roundTT_rank(ATT,ranks)
% ATT = roundTT_rank(ATT,ranks) rounds the ranks of the specified TT to the 
% desired ranks using the deterministic rounding algortihm.
% 
% Source: Tensor-Train Decomposition - I.V. Oseledets (2011)
%
% INPUT
% ATT : TT with sub-optimal ranks
% ranks: vector with desired ranks
% OUTPUT
% ATT : TT with optimal ranks

% Right-to-left orthonormalization
ATT = sitek(ATT,1);

% Save core sizes
d = size(ATT,2);

for k = 1:d
    r(k) = size(ATT{k},1);
    n(k) = size(ATT{k},2);
end

r = [r,1];

% Left-to-right compression
for k = 1:d-1

    AL = reshape(ATT{k},r(k)*n(k),r(k+1));
    [U,S,V] = svd(AL,'econ');
    
    r_old = r(k+1);
    r(k+1) = ranks(k+1);

    U = U(:,1:r(k+1));
    S = S(1:r(k+1),1:r(k+1));
    V = V(:,1:r(k+1));
    
    ATT{k} = reshape(U,r(k),n(k),r(k+1));
    ATT{k+1} = (S*V')*reshape(ATT{k+1},r_old,n(k+1)*r(k+2));
    ATT{k+1} = reshape(ATT{k+1},size(S*V',1),n(k+1),r(k+2));

end
end