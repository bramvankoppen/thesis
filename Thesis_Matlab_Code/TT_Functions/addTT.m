function C = addTT(ATT,BTT)
% C = addTT(ATT,BTT) computes the addition between two TT tensors
% 
% Source: Tensor-Train Decomposition - I.V. Oseledets (2011)
%
% INPUT
% ATT : d-dimensional TT with ranks [R_{0},...,R_{d}]
% BTT : d-dimensional TT with ranks [S_{0},...,S_{d}]
% OUTPUT
% C : d-dimensional TT with ranks [R_{0}+S_{0},...,R_{d}+S_{d}]

% Save length and create cell
d = size(ATT,2);
C = cell(1,d);

% Perform algorithm in three stages
for i = 1:d
    
    % First stage: create first core of C
    if i == 1
    ATT{i} = permute(ATT{i},[1,3,2]);
    BTT{i} = permute(BTT{i},[1,3,2]);
    
    C{i} = permute([ATT{i} BTT{i}],[1,3,2]);
    
    % Second stage: create middle cores, iterate until i = d-1
    elseif (i > 1) && (i < d)
    ATT{i} = permute(ATT{i},[1,3,2]);
    BTT{i} = permute(BTT{i},[1,3,2]);
        
    C{i} = permute([ATT{i} zeros(size(ATT{i}));
                    zeros(size(BTT{i})) BTT{i}],[1,3,2]);
    
    % Final stage: create final core of C
    else  
    C{i} = [ATT{i}; BTT{i}];
    
    end
end
end