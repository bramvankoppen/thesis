function XTT = TTrounding_TSR(YTT,RTT,LTT)
% XTT = TTrounding_TSR(YTT,RTT,LTT) performs the TT-rounding 
% of a given TT A using the TSR randomization method, with prescribed 
% maximum rank(s).

%   Source: Randomized Algorithms for rounding in the Tensor-Train format 
%   - H.Daas, G. Ballard et al.
%
% INPUT
%   YTT : tensor-train with sub-optimal ranks
%   RTT: random Gaussian tensor in TT format
%   LTT: random Gaussian tensor in TT format
% OUTPUT
%   XTT : tensor-train with optimal ranks

% Save TT sizes and ranks
[n,d,rY] = TTsize(YTT);
[~,~,rL] = TTsize(LTT);

WL = PartialContractionsLR(YTT,LTT);
WR = PartialContractionsRL(YTT,RTT);

Lk = cell(d-1,1);
Rk = cell(d-1,1);

for k = 1:d-1
    
    [U,S,V] = svd(WL{k}'*WR{k},'econ');
    
    Lk{k} = WR{k}*V*(pinv(S)^(1/2));
    Rk{k} = (pinv(S)^(1/2))*U'*WL{k}';
    
end

XTT{1} = reshape(YTT{1},[],rY(2))*Lk{1};

for k = 2:d-1
    
    XTT{k} = Rk{k-1}*reshape(reshape(YTT{k},[],rY(k+1))*Lk{k},rY(k),[]);
    XTT{k} = reshape(XTT{k},rL(k),n(k),rL(k+1));
    
end

XTT{d} = Rk{d-1}*reshape(YTT{d},rY(d),[]);

end