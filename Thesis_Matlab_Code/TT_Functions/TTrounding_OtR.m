function XTT = TTrounding_OtR(YTT,r)
% XTT = TTrounding_TSR(YTT,RTT,LTT) performs the TT-rounding 
% of a given TT A using the OtR randomization method, with prescribed 
% maximum rank(s).

%   Source: Randomized Algorithms for rounding in the Tensor-Train format 
%   - H.Daas, G. Ballard et al.
%
% INPUT
%   YTT : tensor-train with sub-optimal ranks
%   RTT: random Gaussian tensor in TT format
%   LTT: random Gaussian tensor in TT format
% OUTPUT
%   XTT : tensor-train with optimal ranks

% Save TT sizes and ranks
[n,d,R] = TTsize(YTT);

% Right-to-left orthonormalization
YTT = sitek(YTT,1);

XTT = cell(1,d);
XTT{1} = YTT{1};

% Perform algorithm
for k = 1:d-1
    
    Z = reshape(XTT{k},[],R(k+1));
    
    Y = Z*randn(R(k+1),r(k+1));
    [XTT{k},~] = qr(Y,0);
    
    M = XTT{k}'*Z;
    
    XTT{k+1} = M*reshape(YTT{k+1},R(k+1),[]);
    XTT{k} = reshape(XTT{k},r(k),n(k),r(k+1));
end

end