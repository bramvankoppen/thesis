function BTT = matrixvec(ATTm,xTT)
% BTT = matrixvec(ATTm,xTT) computes the matrix-vector product of a matrix
% in TTm format and a vector in TT format.
%
% INPUT
% ATTm : a matrix of size in TTm format with dimensions I_{1} x J_{1} x ...
% x I_{d} x J_{d}
% xTT : a vector in TT format with dimensions J_{1} x ... x J_{d}
% OUTPUT
% BTT : a vector in TT format with dimensions I_{1} x ... x I_{d}

d = size(ATTm,2);
BTT = cell(1,d);

for k = 1:d
    
    a = size(ATTm{k});
    b = size(xTT{k});

    if k == d
        a = [a,1];
        b = [b,1];
    end
    
    % Permute ATTm to i1 x i2 x i4 x i3
    % Permute xTT to j2 x j1 x j3
    AT = permute(ATTm{k},[1,2,4,3]);
    xT = permute(xTT{k},[2,1,3]);
    
    % Reshape ATTm to i1i2i4 x i3
    % Reshape xTT to j2 x j1j3
    AT = reshape(AT,[],a(3));                                            
    xT = reshape(xT,b(2),[]);

    % BTT is of size i1i2i4 x j1j3
    BT = AT*xT;
    
    % Reshape BTT to i1 x i2 x i4 x j1 x j3
    BT = reshape(BT,[a(1),a(2),a(4),b(1),b(3)]);
    
    % Permute BTT to i1 x j1 x i2 x i4 x j3
    BT = permute(BT,[1,4,2,3,5]);

    % Reshape BTT to i1j1 x i2 x i4j3
    BTT{k} = reshape(BT,[a(1)*b(1),a(2),a(4)*b(3)]);
end
end