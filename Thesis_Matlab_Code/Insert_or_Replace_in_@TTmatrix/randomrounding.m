function [A] = randomrounding(A,par,RTT,LTT,opt)
%randomrounding(A,par,RTT,LTT,opt) performs the randomized TT-rounding 
%   of a given TT A
%
%INPUT
%   A : tensor-train with sub-optimal ranks
%   par: Maximum desired TT rank
%   RTT: random Gaussian tensor in TT format
%   LTT: random Gaussian tensor in TT format
%   opt : 'OtR', 'RtO' or 'TSR'
%OUTPUT
%   A : tensor-train with optimal ranks

rowDims = A.Size(:,2);
colDims = A.Size(:,3);

ATT = TTm2TT(A);

ATT = randomrounding(ATT,par,RTT,LTT,opt);

A = TT2TTm(ATT,rowDims,colDims);