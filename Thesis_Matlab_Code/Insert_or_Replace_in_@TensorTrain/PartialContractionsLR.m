function W = PartialContractionsLR(A,LTT)
% PartialContractionsLR(A,RTT) computes the left-to-right partial contraction matrices of
% original TT A and randomized TT with desired ranks LTT.
%   Based on paper: Randomized Algorithms for rounding in the Tensor-Train format 
%   - H.Daas, G. Ballard et al.
% INPUT
%   A   : original tensor-train with sub-optimal ranks
%   LTT : randomized tensor-train with optimal ranks
% OUTPUT
%   W   : left-to-right partial contraction matrices 

% Initialize parameters
d = size(A.Size,1);
RA = rank(A);
RL = rank(LTT);

% Compute partial contractions
W = cell(1,d-1);
W{1} = reshape(A.Cores{1},[],RA(2))'*reshape(LTT.Cores{1},[],RL(2));

for k = 2:d-1
        Bk = W{k-1}*reshape(LTT.Cores{k},RL(k),[]);
        Bk = reshape(Bk,[],RL(k+1));
        Ak = reshape(A.Cores{k},[],RA(k+1));
        
        W{k} = Ak'*Bk;
end
end