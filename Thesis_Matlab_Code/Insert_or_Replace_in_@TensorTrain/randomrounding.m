function [A] = randomrounding(A,par,RTT,LTT,opt)
%   randomrounding(A,par,RTT,LTT,opt) performs the TT-rounding 
%   of a given TT A using a randomization method, with prescribed maximum rank(s).
%
%   Source: Randomized Algorithms for rounding in the Tensor-Train format 
%   - H.Daas, G. Ballard et al.
%
% INPUT
%   A : tensor-train with sub-optimal ranks
%   par: Maximum desired TT rank
%   RTT: random Gaussian tensor in TT format
%   LTT: random Gaussian tensor in TT format
%   opt : 'OtR' for Orthogonalize-then-Randomize randomized rounding algorithm
%   opt : 'RtO' for Randomize-then-Orthogonalize randomized rounding algorithm
%   opt : 'TSR' for Two-Sided-Randomization randomized rounding algorithm
% OUTPUT
%   A : tensor-train with optimal ranks

% Save TT size
d = length(A.Size(:,1));

%% Switch between randomized rounding methods
switch opt
    case 'OtR'
        A = orthogonalize(A,1); % Right-to-left orthogonalize TT
        A = roundTT_OtR(A,par,d);
        
    case 'RtO'
        A = roundTT_RtO(A,RTT,d);
        
    case 'TSR'
        A = roundTT_TSR(A,RTT,LTT,d);
end
end

function A = roundTT_OtR(A,Rmax,d)

% Initialize parameters
nA = A.Size(:,2);
rA = rank(A);
R = [1; Rmax*ones(d-1,1);1];    % Define maximum rank vector

% Perform OtR algorithm
for k = 1:d-1
    % Randomization step
    Z = reshape(A.Cores{k},[],rA(k+1));
    Y = Z*randn(rA(k+1),R(k+1));
    [Ak,~] = qr(Y,0);
    M = Ak'*Z;

    % If rank of core is smaller than maximum rank value, save the rank
    rank_Ak = size(Ak,2);
    if rank_Ak < R(k+1) && rank_Ak > 0
        R(k+1) = rank_Ak;
    elseif rank_Ak == 0
        R(k+1) = 1;
    end 
    
    % Compute new cores
    A.Cores{k} = reshape(Ak,R(k),nA(k),R(k+1));
    core = M*reshape(A.Cores{k+1},rA(k+1),[]);
    A.Cores{k+1} = reshape(core,size(M,1),[]);
end

% Update TT size and index norm
A.Size = [R(1:d),nA,R(2:d+1)];
A.indexNorm = d;

end


function A = roundTT_RtO(A,RTT,d)

% Initialize parameters
nA = A.Size(:,2);
rA = rank(A);
rR = rank(RTT);

% Compute right-to-left partial contraction matrices
W = PartialContractionsRL(A,RTT);

% Perform RtO algorithm
for k = 1:d-1
    Z = reshape(A.Cores{k},[],rA(k+1));
    Y = Z*W{k};
    [Ak,~] = qr(Y,0);
    
    % If rank of original core is smaller than maximum rank value, truncate
    % Ak matrix and save the rank
    S1 = size(W{k},1);
    S2 = size(W{k},2);
    if S1 < S2
        Ak = Ak(:,1:S1);
        rR(k+1) = S1;
    end

    M = Ak'*Z;
    
    % Compute new cores
    A.Cores{k} = reshape(Ak,rR(k),nA(k),rR(k+1));
    core = M*reshape(A.Cores{k+1},rA(k+1),[]);
    A.Cores{k+1} = reshape(core,size(M,1),nA(k+1),rA(k+2));
end

% Update TT size and index norm
A.Size = [rR(1:d),nA,rR(2:d+1)];
A.indexNorm = d;

end



function A = roundTT_TSR(A,RTT,LTT,d)

% Initialize parameters
nA = A.Size(:,2);
rA = rank(A);
rL = rank(LTT);

% Compute left-to-right and right-to-left partial contraction matrices
WL = PartialContractionsLR(A,LTT);
WR = PartialContractionsRL(A,RTT);

Lk = cell(1,d-1);
Rk = cell(1,d-1);

% Perform TSR algorithm
% Randomization step
for k = 1:d-1
    [U,S,V] = svd(WL{k}'*WR{k},'econ');
    
    sigma = diag(S);
    rank_Ak = nnz(sigma);
    
    % If rank of original core is smaller than maximum rank value, save
    % rank and truncate SVD matrices
    if rank_Ak < rL(k+1) && rank_Ak > 0
        rL(k+1) = rank_Ak;
    elseif rank_Ak == 0
        rL(k+1) = 1;
    end
    
    Lk{k} = WR{k}*V(:,1:rL(k+1))*(pinv(S(:,1:rL(k+1)))^(1/2));
    Rk{k} = (pinv(S(:,1:rL(k+1)))^(1/2))*U(:,1:rL(k+1))'*WL{k}';
end

% Compute new cores
A.Cores{1} = reshape(A.Cores{1},[],rA(2))*Lk{1};

for k = 2:d-1
    Xk = Rk{k-1}*reshape(reshape(A.Cores{k},[],rA(k+1))*Lk{k},rA(k),[]);
    A.Cores{k} = reshape(Xk,rL(k),nA(k),rL(k+1));
end

A.Cores{d} = Rk{d-1}*reshape(A.Cores{d},rA(d),[]);

% Update TT size and index norm
A.Size = [rL(1:d),nA,rL(2:d+1)];
A.indexNorm = d;

end
