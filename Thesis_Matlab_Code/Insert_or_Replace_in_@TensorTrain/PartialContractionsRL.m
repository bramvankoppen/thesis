function W = PartialContractionsRL(A,RTT)
% PartialContractionsRL(A,RTT) computes the right-to-left partial contraction matrices of
% original TT A and randomized TT with desired ranks RTT.
%   Based on paper: Randomized Algorithms for rounding in the Tensor-Train format 
%   - H.Daas, G. Ballard et al.
% INPUT
%   A   : original tensor-train with sub-optimal ranks
%   RTT : randomized tensor-train with optimal ranks
% OUTPUT
%   W   : right-to-left partial contraction matrices 

% Initialize parameters
d = size(A.Size,1);
RA = rank(A);
RR = rank(RTT);

% Compute partial contractions
W = cell(1,d-1);
W{d-1} = A.Cores{d}*RTT.Cores{d}';

for k = d-1:-1:2  
        Ak = reshape(A.Cores{k},[],RA(k+1))*W{k};
        Ak = reshape(Ak,RA(k),[]);
        Bk = reshape(RTT.Cores{k},RR(k),[]);
        
        W{k-1} = Ak*Bk';
end
end