% This is an experiment file to test the speedup methods proposed in the
% research:
% 
% Improving the Computational Speed of a Tensor Networked Kalman Filter
% for Streaming Video Completion
% 
% By: A.P. van Koppen

clearvars 
close all;
setup; % set up path variable, comment if already done

%% Load variables

v = VideoReader('Campus_480x720.avi'); % load video file
load('q_480x720.mat'); % load quantization parameters: qHeight, qWidth, change for different height/width

% Set parameters
type = 'element';       % Set KF type: 'element', 'block' or 'full'
Tcorrupt = 50;          % set time that frames are corrupted
Tend = Tcorrupt + 1;    % set end of simulation time, must be > Tcorrupt
missingPixels = 0.99;   % set percentage of missing pixels
rankX = 30;             % set rank of x[k] (frames)
rankP = 1;              % set rank of P[k], best to keep it 1
rankXmax = 50;          % maximum rank of X
rankPmax = 5;           % maximum rank of P
bandWidth = 10;         % set bandwidth of W (between 10-20)
rankW = 5;              % set rank of W, depends of bandwidth
foreGround = true;      % use background subtraction [true/false]
round = 'RtO';          % set rounding method to: 'Det','OtR','RtO' or 'TSR'
blockSize = 6;          % set block size
rankCols = 6;           % set maximum rank of extracted column TTm

numRuns = 1; % Set amount of experiment runs
relError = zeros(Tend-Tcorrupt+1,numRuns); % initialize relative error
rFrames = cell(Tend-Tcorrupt+1,numRuns); % initialize reconstructed frames

% Generate random TT's of correct size
n1 = [qHeight,qWidth];
n2 = [5,5,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,5,5];
s_state = 6;            % State oversampling parameter
s_cov = 3;              % covariance oversampling parameter
RTT = generateRandomTT(n1,ceil(s_state*rankX),'x');
LTT = generateRandomTT(n1,rankX,'x');
P_RTT = generateRandomTT(n2,ceil(s_cov*rankP),'P');
P_LTT = generateRandomTT(n2,rankP,'P');

for k = 1:numRuns

profile on % start profiler
    
%% Initialize
video = VideoData(v,Tend,foreGround,Tcorrupt); % create video data, can add backGround

seed = 5; %ensure all simulations have the same seed for creating the mask
rng(seed);

% Set Kalman filter
[options] = setDefaultOptions(video,type,missingPixels,qHeight,qWidth,rankX,rankP,...
    rankW, rankCols, rankXmax, rankPmax, bandWidth, RTT, LTT, P_RTT, P_LTT, blockSize, round);
filt = KalmanFilter(video,options);

%% Simulation
reconVideo = main(video,filt); % use 's' for silent mode

%% End and save profiler results
 
p = profile("info");
profsave(p,"profiler_results")

profile off

%% Save relative error of reconstructed video
relError(:,k) = reconVideo.RelError';

for j = 1:Tend-Tcorrupt+1
  rFrames{j,k} = reconVideo.Frames{j};
end
end

