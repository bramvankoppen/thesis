function Cols = KRextractColsTTm(TTm,Rmax,index)
% KRextractColsTT(TTm,opt,index) extracts one or multiple colums from
% a Matrix in TTm format using a method partly described in the reference below. 
% 
% C. Ko, K. Batselier, L. Daniel, W. Yu, N. Wong - Fast and Accurate Tensor 
% Completion with Total Variation Regularized Tensor Trains (2015).
%
% INPUT
%   TTm : The considered TTm from which the column(s) are extracted.
%   Rmax : Maximum rank of the output TTm (Cols).
%   index : Scalar or vector with the desired vector or matrix indices.
% OUTPUT
%   Cols : The desired columns in TTm format.

% Save sizes of all inputs
l = length(index);
d = ndims(TTm);
R = [1,TTm.Size(:,4)'];
I = TTm.Size(:,2)';
J = TTm.Size(:,3)';
X = zeros(1,d-1);

% Convert indices to selection matrices and save
indexCol = cell(l,d);

for i = 1:l
[indexCol{i,1:d}] = ind2sub(J,index(i));
end
 
indexCol = cell2mat(indexCol);
C = cell(d,1); 
for i = 1:d
    dim = J(i);
    basismat = eye(dim);
    vecs = zeros(dim,l);
    
    for j = 1:l
        vecs(:,j) = basismat(:,indexCol(j,i));
    end
    C{i} = vecs; 
end

% Start column extraction using Khatri-Rao products with selection matrices
Cols = TTmatrix;
K = size(C{1},2);

% First core
core = permute(TTm.Cores{1},[1,2,4,3]);
core = reshape(core,[],J(1))*C{1};

core = krb(C{2},core);
core = reshape(core,I(1),R(2),J(2),K);
core = permute(core,[1,4,2,3]);
core = reshape(core,[],R(2)*J(2));

core2 = permute(TTm.Cores{2},[1,3,2,4]);
core = core*reshape(core2,R(2)*J(2),[]);

core = reshape(core,I(1),K,I(2),R(3));
core = permute(core,[1,3,4,2]);
core = reshape(core,I(1),[]);

[U,S,V] = svd(core,'econ');

sigma = diag(S);
sz = nnz(sigma);

if sz < Rmax
    Rsvd = sz;
else
    Rsvd = Rmax;
end

% Truncate ranks if necessary
U = U(:,1:Rsvd);
S = S(1:Rsvd,1:Rsvd);
V = V(:,1:Rsvd);

X(1) = size(U,2);
Cols.Cores{1} = reshape(U,1,I(1),1,X(1)); % save TTm core
Cols.Size(1,:) = [1,I(1),1,X(1)];

SV = S*V';

% Middle cores
for k = 2:d-1
SV = reshape(SV,X(k-1),I(k),R(k+1),K);
SV = reshape(SV,[],K); 
    
core = krb(C{k+1},SV);
core = reshape(core,X(k-1),I(k),R(k+1),J(k+1),K);
core = permute(core,[1,2,5,3,4]);
core = reshape(core,X(k-1)*I(k)*K,[]);

core2 = permute(TTm.Cores{k+1},[1,3,2,4]);
core = core*reshape(core2,R(k+1)*J(k+1),[]);
core = reshape(core,X(k-1),I(k),K,I(k+1),R(k+2));
core = permute(core,[1,2,4,5,3]);
core = reshape(core,X(k-1)*I(k),[]);

[U,S,V] = svd(core,'econ');

sigma = diag(S);
sz = nnz(sigma);

if sz < Rmax
    Rsvd = sz;
else
    Rsvd = Rmax;
end

% Truncate ranks if necessary
U = U(:,1:Rsvd);
S = S(1:Rsvd,1:Rsvd);
V = V(:,1:Rsvd);

X(k) = size(U,2);
Cols.Cores{k} = reshape(U,X(k-1),I(k),1,X(k));
Cols.Size(k,:) = [X(k-1),I(k),1,X(k)];

SV = S*V';

end

% Last core
Cols.Cores{d} = reshape(SV,X(d-1),I(d),K,1);
Cols.Size(d,:) = [X(d-1),I(d),K,1];

end