function M = KRextractElementsTT(TTm,opt,index)
% KRextractElementsTT(TTm,opt,index) extracts one or multiple elements from
% a vector in TT format or a matrix in TTm format using a method partly 
% described in the reference below. 
% 
% C. Ko, K. Batselier, L. Daniel, W. Yu, N. Wong - Fast and Accurate Tensor 
% Completion with Total Variation Regularized Tensor Trains (2015).
%
% INPUT
%   TTm : The considered TT or TTm from which the value(s) are extracted.
%   opt : 'TT' or 'TTm'.
%   index : Vector with the desired vector or matrix indices.
% OUTPUT
%   M : Vector or matrix containing the desired extracted elements.

switch opt
    case 'TT'
        
        % Save sizes of all inputs
        d = ndims(TTm);     
        R = [1,TTm.Size(:,3)'];
        I = TTm.Size(:,2)';  
        
        % Compute selection matrices
        C = SelectionMatricesTT(TTm,index);
        
        % Start element extraction using Khatri-Rao products with selection
        % matrices
        core = reshape(TTm.Cores{1},I(1),R(2));
        core = core'*C{1};
        core = core';

        for k = 2:d-1
        core = krb(C{k},core');
        core2 = reshape(TTm.Cores{k},R(k)*I(k),[]);
        core = core'*core2;
        end

        core = krb(C{d},core');
        core2 = reshape(TTm.Cores{d},R(d)*I(d),[]);
        
        % Save solution
        M = core'*core2;     

    case 'TTm'
        
        % Save sizes of all inputs
        d = ndims(TTm);
        R = [1,TTm.Size(:,4)'];
        I = TTm.Size(:,2)';
        J = TTm.Size(:,3)';

        % Compute selection matrices
        C = SelectionMatricesTTm(TTm,index);
        
        % Start element extraction using Khatri-Rao products with selection
        % matrices
        K = size(C{1},2);
        C1 = kron(C{1},C{1});

        core = permute(TTm.Cores{1},[1,4,2,3]);
        core = reshape(core,R(1)*R(2),I(1)*J(1))*C1;

        for i = 2:d-1
        krc = kron(C{i},C{i});
        core = krb(krc,core);

        core2 = permute(TTm.Cores{i},[4,1,2,3]);
        core2 = reshape(core2,R(i+1),R(i)*I(i)*J(i));
        core = core2*core;
        end
        
        core = krb(kron(C{d},C{d}),core);
        core = core'*reshape(TTm.Cores{d},[],1);
        
        % Save solution
        M = reshape(core,K,K);
end
end