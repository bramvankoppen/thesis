function S = SelectionMatricesTT(TT,index)
% SelectionMatricesTT(TT,index) computes the selection matrices used to
% extract values from a vector in TT format using a method described 
% in the reference below.  

% C. Ko, K. Batselier, L. Daniel, W. Yu, N. Wong - Fast and Accurate Tensor 
% Completion with Total Variation Regularized Tensor Trains (2015).

% INPUT
%   TT : The considered TT from which the value(s) are extracted.
%   index : Vector with the desired vector or matrix indices.
% OUTPUT
%   S : Cell containing the selection matrices.

% Save sizes of all inputs
l = length(index);
d = ndims(TT);     
I = TT.Size(:,2)';  

% Convert indices to selection matrices and save
indexCol = cell(l,d);

for i = 1:l
[indexCol{i,1:d}] = ind2sub(I,index(i));
end

indexCol = cell2mat(indexCol);
S = cell(d,1); 
for i = 1:d
    dim = I(i);
    basismat = eye(dim);
    vecs = zeros(dim,l);

    for j = 1:l
        vecs(:,j) = basismat(:,indexCol(j,i));
    end
    S{i} = vecs; 
end
end