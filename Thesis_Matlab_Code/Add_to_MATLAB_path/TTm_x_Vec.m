function TT = TTm_x_Vec(TTm,Vec)
% This function computes the product of a matrix in TTm format and a vector
% in standard format by contracting the vector with the last core of the TTm.
% This requires the last core of the TTm to have one inner dimension of 1.

d = ndims(TTm);
l = length(Vec);

% Save solution as Tensor Train
TT = TensorTrain;
TT.Cores = cell(d,1);

for i = 1:d-1
TT.Cores{i} = reshape(TTm.Cores{i},TTm.Size(i,1),TTm.Size(i,2),TTm.Size(i,4));
TT.Size(i,:) = [TTm.Size(i,1),TTm.Size(i,2),TTm.Size(i,4)];
end

% Contract last TTm core with vector
TT.Cores{d} = reshape(TTm.Cores{d},[],TTm.Size(d,3))*Vec;
TT.Cores{d} = reshape(TT.Cores{d},TTm.Size(d-1,4),[]);
TT.Size(d,:) = [TTm.Size(d-1,4),l,1];

end