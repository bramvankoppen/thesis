function RTT = generateRandomTT(n,rmax,opt)

    switch opt
        case 'x'
            
            R = randn(n);
            RTT = TT_SVD_rank(R,rmax);
    
        case 'P'
        
            d = length(n);
            R = rand(n(1:d/2));
            L = rand(n((d/2)+1:end));
            
            P1 = Tens2TTm(R,rmax,'r');
            P2 = Tens2TTm(L,rmax,'r');
            
            P = tkron(P2,P1);
            RTT = TTm2TT(P);
    end     
end