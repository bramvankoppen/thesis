function [x,P,W_new] = BlockUpdateKF(x,y,C,P,W_old,Options)
% BlockUpdateKF(x,y,C,P_old,W,Options) calculates the Kalman update 
%   for the given parameters. In this function the parameters are given as 
%   tensor-trains.
%
%INPUT
%   x : state-vector (as tensor-train).
%   y : measured pixels (of this time-step).
%   C : measurement indices.
%   P : covariance matrix previous time-step.
%   W_old : process covariance matrix.
%   Options : Kalman filter Options struct.
%OUTPUT
%   x : updated state-vector.
%   P : updated covariance matrix.
%   W_new : (updated) process covariance.

%% Set Options
rankX = Options.Rank.x;
rankP = Options.Rank.P;
szX = size(x);
lengthX = prod(szX(:,2));
RTT = Options.R.TT;
LTT = Options.L.TT;
P_RTT = Options.P.RTT;
P_LTT = Options.P.LTT;
blockSize = Options.Block.Size;
index = zeros(1,blockSize);
numOutputs = length(C);
numBlocks = numOutputs/blockSize;
qH = Options.q.Height;
qW = Options.q.Width;
qx = [qH,qW];
qBlock = ones(1,length(qx));
qBlock(end) = blockSize;

roundingMethod = Options.rounding.method;
rankCols = Options.ranks.cols;

if Options.W.Update
    % copy data
    W_new.W0 = W_old.W0;
    % calculate W
    W_old = W_old.varW * W_old.W0;
end

%% Prediction step
x_old = x;
P = P + W_old;
P = rounding(P,rankP,'r');

for ii = 1:numBlocks-1
    
    % Determine block position
    blockPos = (ii-1)*blockSize;
    
    % Extract state and covariance elements
    for k = 1:blockSize
        index(1,k) = C(blockPos+k);        
    end
    
    x_ii = KRextractElementsTT(x,'TT',index);
    P_ii = KRextractElementsTT(P,'TTm',index);
    
    %% Prediction
    % output residual
    v = y(blockPos+1:blockPos+blockSize,1) - x_ii;  % blockSize x 1
        
    % Kalman steps
    S = Mat2TTm(inv(P_ii),qBlock,qBlock,1e-15,'e'); % blockSize x blockSize
    Pcols = KRextractColsTTm(P,rankCols,index); % MN x blockSize
    K = Pcols*S; % MN x blockSize
    KSK = -(1)*K*S*K'; % MN x MN

    %% Update (summation)
    x = x + TTm_x_Vec(K,v);
    P = P + KSK;
        
% Rounding procedure
switch roundingMethod
    case 'Det'
    if any(rank(x)>Options.Rank.maxX)   
       x = rounding(x,rankX,'r');                      % Deterministic rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = rounding(P,rankP,'r');                      % Deterministic rounding
    end
    
    case 'OtR'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,rankX,[],[],'OtR');      % Randomized rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,rankP,[],[],'OtR');  % Randomized rounding
    end

    case 'RtO'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,[],LTT,[],'RtO');      % Randomized rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,[],P_LTT,[],'RtO');  % Randomized rounding
    end

    case 'TSR'
    if any(rank(x)>Options.Rank.maxX)   
       x = randomrounding(x,[],RTT,LTT,'TSR');      % Randomized rounding
    end
    if any(rank(P)>Options.Rank.maxP) 
       P = randomrounding(P,[],P_RTT,P_LTT,'TSR');  % Randomized rounding
    end

end
end
x = rounding(x,rankX,'r');
P = rounding(P,rankP,'r');

if Options.W.Update
    % save new W
    warning off;
    W_new.varW = (1/(lengthX-1))*norm(x-x_old)^2;
    warning on;
else 
    W_new = W_old;
end

end
