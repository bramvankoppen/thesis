# Thesis

This is the Git repository for my thesis entitled ‘Improving the Computational Speed of a Tensor-Networked Kalman Filter for Streaming Video Completion’. The thesis and its literature study can be found in the folder ‘Thesis_Documents’. 

 In order to run the MATLAB code in the folder ’Thesis_Matlab_Code’, several steps must be taken. 

1. The code written by S.J.S. de Rooij for the research entitled: ‘Streaming Video Completion using a Tensor-Networked Kalman Filter’ must be downloaded from the following public GitLab repository: https://gitlab.com/seline/thesis . From here, the folder ‘Matlab_Code’ must be downloaded. All code in this folder must be added to your MATLAB path before running the code.

2. In the folder ‘Thesis_Matlab_Code’, there are folders with code that must be inserted or replaced in the downloaded code by S.J.S. de Rooij. The names of these folders contain the specific instructions, by specifying the name of the considered folder in ‘Matlab_Code’ by S.J.S. de Rooij. An example: the folder ‘Insert_or_replace_in_@TTmatrix’ contains two files, ‘randomrounding.m’ and ‘TTmatrix.m’. The file ‘randomrounding.m’ must be inserted in the folder ‘Matlab_Code > @TTmatrix’, as this file is not yet present in that folder. The file ‘TTmatrix.m’ is already present, but needs to be replaced in the folder ‘Matlab_Code > @TTmatrix’ as it is modified.

3. In the folder ‘Thesis_Matlab_Code’, there is one folder of which the files must be added to your MATLAB path: ‘Add_to_MATLAB_path’ . In this folder, the script ‘RunExperiment.m’ can be used to test the implemented speedup methods for both included data sets: ‘Campus_480x720.avi’ and ‘GCS_480x720.avi’. The script 'RunExperiment.m' must be moved to the 'Matlab_Code' folder before running to prevent issues.

The folders ‘Tensor_Functions_BramvanKoppen’ and ‘TT_Functions_BramvanKoppen’ contain functions to perform operations with Tensors and TT’s. These functions are not used in the Tensor-Networked Kalman Filter, but have been written during my thesis.
